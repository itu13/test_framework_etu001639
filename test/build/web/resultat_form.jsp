
<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.Date"%>
<%@page import="model.Fruit"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <% Fruit fruit=(Fruit)request.getAttribute("fruit");
            Double price=(Double)request.getAttribute("price");
            Date date=(Date)request.getAttribute("date_peremption");
            out.println("<p>fruit: "+fruit+"</p>");
            out.println("<p>price: "+price+"</p>");
            out.println("<p>date peremption: "+date+"</p>");
            
            ArrayList<String>conso=(ArrayList<String>)request.getAttribute("preference");
            out.println("<p>preference de consommation: ");
            if(conso!=null){
                for(int i=0;i<conso.size();i++){
                    out.println(conso.get(i)+", ");
                }
            }
            out.println("</p>");
        %>
    </body>
</html>
