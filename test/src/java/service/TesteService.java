package service;

import framework.annotation.Front;
import java.io.File;
import java.lang.reflect.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import model.Fruit;
import framework.utilitaire.ModelView;
import framework.utilitaire.UrlMethod;

public class TesteService {   
    private Fruit nouveau;
    private Date peremption;
    private Double prix;
    private ArrayList<String> consommation;

    public Fruit getNouveau() {
        return nouveau;
    }

    public void setNouveau(Fruit nouveau) {
        this.nouveau = nouveau;
    }
            
            
    @Front(url="afficher_fruit")
    public ModelView afficher_fruit(){
        ArrayList<Fruit>liste=new ArrayList();
        liste.add(new Fruit("banane", 2));
        liste.add(new Fruit("mangue",3));
        ModelView mv=new ModelView("affichefruit.jsp");
        mv.addData("liste_fruit", liste);
        return mv;
    }
    
    @Front(url="form_fruit")
    public ModelView form_fruit(){
        ModelView mv=new ModelView("formulaire_fruit.jsp");
        return mv;
    }
    
    @Front(url="resultform_fruit")
    public ModelView resultform_fruit(){
        ModelView mv=new ModelView("resultat_form.jsp");        
        mv.addData("fruit", nouveau);
        mv.addData("date_peremption", peremption);
        mv.addData("price", prix);
        mv.addData("preference", consommation);
        return mv;
    }
    
    
}
