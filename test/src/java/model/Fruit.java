/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author randr
 */
public class Fruit {
    private String nom;
    private Integer nombre;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Integer getNombre() {
        return nombre;
    }

    public void setNombre(Integer nombre) {
        this.nombre = nombre;
    }

    public Fruit(String nom, Integer nombre) {
        this.nom = nom;
        this.nombre = nombre;
    }

    public Fruit() {
    }
    public String toString(){
         return nom+", "+nombre;
    }
}
