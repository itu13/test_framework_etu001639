<%-- 
    Document   : affichefruit
    Created on : 15 nov. 2022, 21:07:16
    Author     : randr
--%>

<%@page import="model.Fruit"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Liste des fruits</h1>
        <ul>
            <% if(request.getAttribute("liste_fruit")!=null){
                ArrayList<Fruit>liste=(ArrayList<Fruit>)request.getAttribute("liste_fruit");
                for(int i=0;i<liste.size();i++){
                    out.println("<li>"+liste.get(i).getNom()+" , nombre: "+liste.get(i).getNombre());
                } 
            }
            %>
        </ul>
    </body>
</html>
