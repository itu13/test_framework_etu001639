ETU001639- RANDRIANARISON Mendrika Ny Aina Fitahiana

Configuration requise: 
	ajouter le jar(jar/framework_web.jar) dans votre projet
	ajouter dans le web.xml:
		<servlet>
        		<servlet-name>FrontServlet</servlet-name>
        		<servlet-class>framework.controller.FrontServlet</servlet-class>
    		</servlet>
    
    		<servlet-mapping>
        		<servlet-name>FrontServlet</servlet-name>
        		<url-pattern>*.do</url-pattern>
    		</servlet-mapping>

Fonctionnement
	utiliser l'annotation @Front et specifier l'url de la methode, 
			la methode doit rendre un objet de Type ModelView, 
			on peut faire passer des données via la fonction addData(nom, objet) de ModelView,
			on peut recuperer les parametres de venant de la requetes via les attributs de la class ou il y a la methode, 
				le nom des parametres doit etre le meme que pour les attributs de la class, 
				les parametres peuvent etre des chaines de caractères, des entiers, double, float, date, character, 
					liste de String, entiers, double, float, date, character
					simple objet(comme Employe par example), pour les simple objet, par exemple on a une classe Employe avec les attribut nom et age,
					et on a un attribut emp de type Employe dans la classe qui contient la methode avec l'url, le nom des attributs du champs du formulaire doit donc etre
					emp.nom et emp.age par convention;
			

Les url a tester:
	aller dans form_fruit.do, il y aura un formulaire a remplir, un objet fruit avec les attribut nom et nombre, un prix(double), une date de peremption, une checkbox de preference de consommation(arraylist de string)
		puis valider et ca va rediriger vers un resultform_fruit.do et afficher les informations



			
	
		
